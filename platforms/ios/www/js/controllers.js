angular.module('app.controllers', [])

.controller('AppCtrl', function($scope, AppService, $ionicModal, $state, $interval, $ionicModal) {

  $scope.userInfo = AppService.getUserInfo()
  $scope.$parent.upiInfo = {}

  $scope.logout = function() {
    AppService.logout()
    $state.go('login')
  }

  $scope.getDirection = function(long, lat) {
    var destination = [lat, long]
    AppService.getLocation(destination)
      // ('{UPI}', '{NO_JKPTG}', '{NAMA_NEGERI}', '{NAMA_DAERAH}', '{NAMA_MUKIM}', '{GUNA_TANAH}', '{KEMENTERIAN}', '{LAT}', '{LONG}')
      // go to goole map
  }

  $scope.moreInfo = function(UPI, NO_JKPTG, NAMA_NEGERI, NAMA_DAERAH, NAMA_MUKIM, GUNA_TANAH, KEMENTERIAN) {
    $scope.modalInfo = {};
    $scope.modalInfo.UPI = UPI
    $scope.modalInfo.NO_JKPTG = NO_JKPTG
    $scope.modalInfo.NAMA_NEGERI = NAMA_NEGERI
    $scope.modalInfo.NAMA_DAERAH = NAMA_DAERAH
    $scope.modalInfo.NAMA_MUKIM = NAMA_MUKIM
    $scope.modalInfo.GUNA_TANAH = GUNA_TANAH
    $scope.modalInfo.KEMENTERIAN = KEMENTERIAN

    $ionicModal.fromTemplateUrl('templates/mymodal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });

    // show pop up with
  }

  $scope.close = function() {
    $scope.modal.hide()
  }

  var template = { // autocasts as new PopupTemplate()
    title: '{GUNA_TANAH}',
    content: ` <div id="popupTemplateDiv" ng-app="app" ng-controller="AppCtrl">
      <div class="button-bar">
        <a class="button button-calm" style="border-right: 1px gray solid;" onClick="(function() &#123; angular.element(document.getElementById('popupTemplateDiv')).scope().getDirection({LONG},{LAT});return false; \})();return false;">Direction</a>
        <a class="button button-calm" onClick="(function() &#123; angular.element(document.getElementById('popupTemplateDiv')).scope().moreInfo('{UPI}', '{NO_JKPTG}', '{NAMA_NEGERI}', '{NAMA_DAERAH}', '{NAMA_MUKIM}', '{GUNA_TANAH}', '{KEMENTERIAN}');return false; \})();return false;">Info</a>
      </div>
    </div>
`
  }

  require([
    'esri/Map',
    'esri/views/SceneView',
    'esri/layers/FeatureLayer',
    'esri/widgets/BasemapGallery',
    'esri/widgets/Search',
    'esri/widgets/Locate',
    'esri/config',
    'dojo/domReady!'
  ], mapDraw)
  var map = null
  var view = null
  var FeatureLayer = null
  var BasemapGallery = null
  var InitZoom = 4.3

  function mapDraw(Map, SceneView, _FeatureLayer, _BasemapGallery, Search, Locate, esriConfig) {
    FeatureLayer = _FeatureLayer
    BasemapGallery = _BasemapGallery
    esriConfig.request.timeout = 3 * 60000
      // esriconfig.defaults.io.timeout = 300000
    map = new Map({
      basemap: 'hybrid'
    })

    AppService.currentLocation()
      .then(function(coordinate) {
        view = new SceneView({
          container: 'viewDiv',
          map: map,
          center: [109.1162, 3.0308],
          zoom: InitZoom
        })

        view.popup.dockOptions = {
          position: 'top-center',
          breakpoint: false,

        }
        var searchWidget = new Search({
          view: view
        })

        var locateBtn = new Locate({
          view: view
        })

        // Add the locate widget to the top left corner of the view
        view.ui.add(locateBtn, {
          position: 'top-left'
        })

        view.ui.add(searchWidget, {
          position: 'top-left',
          index: 0
        })
      })
  }

  var stopLink2Ng = null

  function linkToNg() {
    stopLink2Ng = $interval(function linkNg() {
      var popUp = angular.element(document.getElementById('popupTemplateDiv')).scope()
      if (popUp) {
        // try { popUp.$apply() } catch (_e) {}
        $interval.cancel(stopLink2Ng)
      }
    }, 100)
  }

  function updateMapLayers(itemsIds) {
    for (var i = 0; i < itemsIds.length; i++) {
      var featureLayer = new FeatureLayer({
        url: 'https://www.gis.myetapp.gov.my/arcgis/rest/services/JKPTG2/charting/MapServer',
        layerId: itemsIds[i],
        outFields: ['UPI', 'NO_JKPTG', 'NAMA_NEGERI', 'NAMA_DAERAH', 'NAMA_MUKIM', 'GUNA_TANAH', 'KEMENTERIAN', 'LAT', 'LONG'],
        popupEnabled: true,
        popupTemplate: template
      })

      map.add(featureLayer)
    }
    if (stopLink2Ng) {
      $interval.cancel(stopLink2Ng)
    }
    linkToNg()
  }

  $scope.chooseLayers = function() {
    AppService.loadChooseLayers()
      .then(function resp(data) {
        AppService.chooseLayers(data)
          .then(function(loaded) {
            if (!loaded) return
            map.removeAll()
            if ($scope.$parent.choosedLayerData && $scope.$parent.choosedLayerData.length > 0) {
              updateMapLayers($scope.$parent.choosedLayerData)
            }
          })
      })
  }

  $scope.removeAllLayers = function() {
    $scope.$parent.choosedLayerData = []
    map.removeAll()
    AppService.removeAllLayers()
  }

  $scope.BasemapGallery = function() {
    view.ui.empty('top-right')
    var basemapGallery = new BasemapGallery({
      view: view
    })

    // Add the widget to the top-right corner of the view
    view.ui.add(basemapGallery, {
      position: 'top-right'
    })

    view.on('click', function(event) {
      view.ui.empty('top-right')
    })
  }

  $scope.searchByCoordinate = function() {
    AppService.searchByCoordinate()
      .then(function(loaded) {
        if (!loaded) return
        view.goTo({
          center: [$scope.$parent.searchLongitude, $scope.$parent.searchLatitude],
          zoom: 10
        })
      })
  }

  $scope.searchByUpi = function() {
    $scope.$parent.upiInfo = {}
    AppService.loadChooseLayers()
      .then(function resp(layers) {
        $scope.$parent.upiInfo.layers = layers;

        AppService.searchByUpi()
          .then(function(ok) {
            if (!ok || !$scope.$parent.upiInfo.selectedUPI) return;

            AppService.searchUPI($scope.$parent.upiInfo.selectedUPI, $scope.$parent.upiInfo.selectedLayer)
              .then(function(attrs) {
                view.goTo({
                  center: [_.toNumber(attrs.LONG), _.toNumber(attrs.LAT)],
                  zoom: 10
                })
              })
          })
      });
  }

  $scope.home = function() {
    map.removeAll()
    $scope.$parent.choosedLayerData = [];
    view.zoom = InitZoom
    map.basemap = 'hybrid'
  }

  $scope.$parent.changeLayer = function() {
    AppService.queryUpi({
        layer: $scope.$parent.upiInfo.selectedLayer
      })
      .then(function(negeriList) {
        $scope.$parent.upiInfo.negeris = negeriList;
      })
  }

  $scope.$parent.changeNegeri = function() {
    AppService.queryUpi({
        layer: $scope.$parent.upiInfo.selectedLayer,
        negeri: $scope.$parent.upiInfo.selectedNegeri,
      })
      .then(function(daerahList) {
        $scope.$parent.upiInfo.daerahs = daerahList;
      })
  }

  $scope.$parent.changeDaerahs = function() {
    AppService.queryUpi({
        layer: $scope.$parent.upiInfo.selectedLayer,
        negeri: $scope.$parent.upiInfo.selectedNegeri,
        daerah: $scope.$parent.upiInfo.selectedDaerahs,
      })
      .then(function(mukimList) {
        $scope.$parent.upiInfo.mukims = mukimList;
      })
  }

  $scope.$parent.changeMukims = function() {
    AppService.queryUpi({
        layer: $scope.$parent.upiInfo.selectedLayer,
        negeri: $scope.$parent.upiInfo.selectedNegeri,
        daerah: $scope.$parent.upiInfo.selectedDaerahs,
        mukim: $scope.$parent.upiInfo.selectedMukims,
      })
      .then(function(results) {

      })
  }

})

.controller('LoginCtrl', function($scope, $state, AppService) {
  $scope.data = {}
  $scope.login = function() {
    AppService.login($scope.data)
      .then(function(success) {
        if (success) {
          $scope.data = {}
          return $state.go('app.main')
        } else {
          return AppService.wrongPass()
        }
      })
  }
})

.controller('MainCtrl', function($scope, $stateParams) {})