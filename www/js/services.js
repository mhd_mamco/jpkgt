angular.module('app.services', [])

.factory('AppService', function($ionicPopup, $http, $q, $cordovaGeolocation, $cordovaLaunchNavigator, $ionicLoadings) {
  var _userData = {}
  var _layers = []
  var appService = {}

  appService.loadChooseLayers = function loadChooseLayers() {
    return new Promise(function(resolve, reject) {
      if (!_.isEmpty(_layers)) {
        return resolve(_layers)
      }

      var baseURI = 'https://www.gis.myetapp.gov.my/arcgis/rest/services/JKPTG2/charting/MapServer'
      return $http.get(baseURI + '?f=pjson')
        .then(function resp(res) {
          var layers = []

          var promises = []
          _.each(res.data.layers, function(layer) {
            var deferred = $q.defer()
            promises.push(deferred.promise)
            $http.get(baseURI + '/' + layer.id + '?f=pjson')
              .then(function(layerInfo) {
                layer.backgroundStyle = 'rgba(' + layerInfo.data.drawingInfo.renderer.symbol.color[0] + ',' + layerInfo.data.drawingInfo.renderer.symbol.color[1] + ',' + layerInfo.data.drawingInfo.renderer.symbol.color[2] + ',' + layerInfo.data.drawingInfo.renderer.symbol.color[3] + ')'
                layers.push(layer)
                deferred.resolve()
              })
          })

          return $q.all(promises).then(function() {
            layers = _.sortBy(layers, ['id'])
            _layers = layers
            return resolve(layers)
          })
        })
    })
  }

  appService.removeAllLayers = function removeAllLayers() {
    _layers = []
  }

  appService.chooseLayers = function chooseLayers(listLayers) {
    var showConfirm = $ionicPopup.confirm({
      title: 'Choose Layers',
      template: `<form class='chooseLayers'>
        <div style="margin-bottom: 10px;"> <label>
          <input type="checkbox" name="layer" checklist-model="$parent.choosedLayerData" checklist-value="${listLayers[0].id}">${listLayers[0].name} <span style="  min-width: 20px;min-height: 20px;right: 20%;position: absolute; background-color: ${listLayers[0].backgroundStyle};" > </span></label> </div>
        <div style="margin-bottom: 10px;"> <label>
          <input type="checkbox" name="layer" checklist-model="$parent.choosedLayerData" checklist-value="${listLayers[1].id}">${listLayers[1].name} <span style="  min-width: 20px;min-height: 20px;right: 20%;position: absolute; background-color: ${listLayers[1].backgroundStyle};" > </span></label> </div>
        <div style="margin-bottom: 10px;"> <label>
          <input type="checkbox" name="layer" checklist-model="$parent.choosedLayerData" checklist-value="${listLayers[2].id}">${listLayers[2].name} <span style="  min-width: 20px;min-height: 20px;right: 20%;position: absolute; background-color: ${listLayers[2].backgroundStyle};" > </span></label> </div>
        <div style="margin-bottom: 10px;"> <label>  
          <input type="checkbox" name="layer" checklist-model="$parent.choosedLayerData" checklist-value="${listLayers[3].id}">${listLayers[3].name} <span style="  min-width: 20px;min-height: 20px;right: 20%;position: absolute; background-color: ${listLayers[3].backgroundStyle};" > </span></label> </div>
        `
    })

    return showConfirm.then(function(res) {
      return res
    })
  }

  appService.wrongPass = function wrongPass() {
    return $ionicPopup.alert({
      title: 'Wrong credentials',
      template: 'email and/or password is invalid'
    })
  }

  appService.searchByCoordinate = function searchByCoordinate() {
    var showConfirm = $ionicPopup.confirm({
      title: 'Coordinates',
      template: `
        <div class="list">
          <label class="item item-input">
            <input type="number" placeholder="longitude" ng-model="$parent.searchLongitude">
          </label>
          <label class="item item-input">
            <input type="number" placeholder="latitude" ng-model="$parent.searchLatitude">
          </label>
        </div>        
        `
    })

    return showConfirm.then(function(res) {
      return res
    })
  }

  appService.searchByUpi = function searchByCoordinate() {
    var showConfirm = $ionicPopup.confirm({
      title: 'UPI Search',
      template: `
        <div class="list">
          <label class="item item-input item-stacked-label">
            <span class="input-label">Layers</span>
            <select class="upi-dropdown" name="layer" id="layer" ng-model="$parent.upiInfo.selectedLayer" ng-options="opt.name as opt.name for opt in $parent.upiInfo.layers" ng-change="$parent.changeLayer()"></select>
            

          </label>
          
          <label class="item item-input item-stacked-label">
            <span class="input-label">Negeri</span>
            <select class="upi-dropdown" name="layer" id="layer" ng-model="$parent.upiInfo.selectedNegeri" ng-options="opt.NAMA_NEGERI as opt.NAMA_NEGERI for opt in $parent.upiInfo.negeris" ng-change="$parent.changeNegeri()"></select>

          </label>
          
          <label class="item item-input item-stacked-label">
            <span class="input-label">Daerah</span>
            <select class="upi-dropdown" name="layer" id="layer" ng-model="$parent.upiInfo.selectedDaerahs" ng-options="opt.NAMA_DAERAH as opt.NAMA_DAERAH for opt in $parent.upiInfo.daerahs" ng-change="$parent.changeDaerahs()"></select>
          </label>
          
          <label class="item item-input item-stacked-label">
            <span class="input-label">Mukim</span>
            <select class="upi-dropdown" name="layer" id="layer" ng-model="$parent.upiInfo.selectedMukims" ng-options="opt.NAMA_MUKIM as opt.NAMA_MUKIM for opt in $parent.upiInfo.mukims" ng-change="$parent.changeMukims()"></select>
          </label>


          <label class="item item-input item-stacked-label">
            <span class="input-label" ng-if="$parent.upiInfo.selectedLayer === 'PERMOHONAN' || !$parent.upiInfo.selectedLayer">NO JKPTG</span>
            <span class="input-label" ng-if="$parent.upiInfo.selectedLayer === 'QT'">HMLK SMTARA</span>
            <span class="input-label" ng-if="$parent.upiInfo.selectedLayer === 'FT'">NO LOT</span>
            <span class="input-label" ng-if="$parent.upiInfo.selectedLayer === 'RIZAB'">UPI</span>
            <input class="upi-dropdown"  type="text" placeholder="UPI" ng-model="$parent.upiInfo.selectedUPI">
          </label>

        </div>        
        `
    })

    return showConfirm.then(function(res) {
      return res
    })
  }

  appService.searchUPI = function(upi, layer) {
    var url = 'https://www.gis.myetapp.gov.my/arcgis/rest/services/JKPTG2/charting/MapServer/find?searchText=' + upi + '&contains=true&searchFields='
    switch (layer) {
      case 'PERMOHONAN': // PERMOHONAN
        url = url + 'NO_JKPTG'
        break;

      case 'QT': // QT
        url = url + 'HMLK_SMTARA'
        break;

      case 'FT': // FT
        url = url + 'NO_LOT'
        break;

      case 'RIZAB': // RIZAB
        url = url + 'UPI'
        break;

      default:
        return null;
    }

    url = url + '&sr=&layers=0%2C1%2C2%2C3&layerDefs=&returnGeometry=true&maxAllowableOffset=&geometryPrecision=&dynamicLayers=&returnZ=false&returnM=false&gdbVersion=&returnUnformattedValues=false&returnFieldName=false&datumTransformations=&layerParameterValues=&mapRangeValues=&layerRangeValues=&f=json';
    console.log(url);

    return $http.get(url)
      .then(function(res) {
        console.log(res);
        return (res.data.results.length > 0) ? res.data.results[0].attributes : {}
      })
  }

  appService.queryUpi = function queryUpi(data) {
       $ionicLoading.show({
      template: 'Please Wait...'
    })
    var req = {
      method: 'POST',
      url: 'https://www.gis.myetapp.gov.my/WebGIS-SOAP/permohonan-search.php ',
      headers: {
        'cache-control': 'no-cache',
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        layer: data.layer,
        negeri: data.negeri || '0',
        daerah: data.daerah || '0',
        mukim: data.mukim || '0'
      },
      transformRequest: function(obj) {
        var str = []
        for (var p in obj) {
          str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]))
        }
        return str.join('&')
      }
    }

    return $http(req)
      .then(function resp(res) {
        $ionicLoading.hide()
        return res.data.item
      }, function error(err) {
        console.log(err)
        $ionicLoading.hide()
      })
  }

  appService.login = function login(data) {
    var req = {
      method: 'POST',
      url: 'https://www.gis.myetapp.gov.my/WebGIS-SOAP/user.php',
      headers: {
        'cache-control': 'no-cache',
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        email: data.email,
        password: data.pass
      },
      transformRequest: function(obj) {
        var str = []
        for (var p in obj) {
          str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]))
        }
        return str.join('&')
      }
    }

    return $http(req)
      .then(function resp(res) {
        if (res.data.status === 'success') {
          _userData.name = res.data.name
          _userData.email = res.data.email
          _userData.state = res.data.state
          return true
        }
        return false
      })
  }

  appService.getUserInfo = function getUserInfo() {
    return _userData
  }

  appService.logout = function logout() {
    _userData = {}
  }

  appService.currentLocation = function currentLocation() {
    var posOptions = {
      timeout: 10000,
      enableHighAccuracy: false
    }
    return $cordovaGeolocation
      .getCurrentPosition(posOptions)
      .then(function(position) {
        return {
          lat: position.coords.latitude,
          long: position.coords.longitude
        }
      }, function(err) {
        return null
      })
  }

  appService.getLocation = function getLocation(destination) {
    appService.currentLocation()
      .then(function(start) {
        $cordovaLaunchNavigator.navigate(destination, start).then(function() {
          console.log('Navigator launched')
        }, function(err) {
          console.error(err)
        })
      })
  }

  return appService
})